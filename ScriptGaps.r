#Online Appendix 5 in Zapata F., Jimenez I. Species Delimitation in Systematics: Inferring Gaps in Morphology across Geography.
#R script for inferring gaps in morphology.

#Load libraries.
library(labdsv)
library(ellipse)
library(mvtnorm)

#Load data for example A (or for example B by changing line below to read file "OnlineAppendix4_data_example_B.csv").
data <- read.csv(file="data_example_A.csv", sep=",", header=T)

#Run principal component analysis on correlation matrix (i.e., on standardized variables), selecting the appropriate columns in the original data matrix. For example B, change line below to select the variables from columns 14 to 40.
PA <- pca(data[, (14:36)], cor = T)

#Extract PC scores and bind them to the data matrix.
data <- cbind(data, as.data.frame(PA$scores))

#Assign codes to the two species to be compared (e.g., 1 and 2).
SPA <- 2
SPB <- 1

#Assign colors to each species for visualization.
col_SPA<-c("red")
col_SPB<-c("blue")

#Calculate sample size, mean and variance-covariance matrix for SPA.
n_SPA<-length(data$PC1[data$SP==SPA])
m1 <- array(c(mean(data$PC1[data$SP==SPA]), mean(data$PC2[data$SP==SPA])), dim=c(2,1))
Z1<-var(cbind(data$PC1[data$SP==SPA], data$PC2[data$SP==SPA]))

#Calculate sample size, mean and variance-covariance matrix for SPB.
n_SPB<-length(data$PC1[data$SP==SPB])
m2 <- array(c(mean(data$PC1[data$SP==SPB]), mean(data$PC2[data$SP==SPB])), dim=c(2,1))
Z2<-var(cbind(data$PC1[data$SP==SPB], data$PC2[data$SP==SPB]))

#Calculate ridgeline manifold. The equation for the manifold (i.e., d<-a %*% b) from equation 4 in Ray and Lindsay (2005:2045) is evaluated at various values of alfa and the resulting coordinates (in PC1 and PC2 space) are captured in vectors x and y.
x<-c()
y<-c()
alfa<-seq(0,1,0.001)
for (i in 1:length(alfa)){
a<-solve((1-alfa[i])*solve(Z1)  +  alfa[i]*solve(Z2))
b<-(1-alfa[i])*solve(Z1)%*%m1  +  alfa[i]*solve(Z2)%*%m2
d<-a %*% b
x<-append(x, d[1], after=length(x))
y<-append(y, d[2], after=length(y))
}

#Plot PC1 and PC2, the bivariate means of the two species to be compared and the respective ridgeline manifold.
par(mar=c(5,5,2,2))
plot(data$PC1, data$PC2, cex=2, xlab="PC1", ylab="PC2", cex.lab=1.5, cex.axis=1.5)
points(data$PC1[data$SP==SPA], data$PC2[data$SP==SPA], cex=2, col=col_SPA)
points(data$PC1[data$SP==SPB], data$PC2[data$SP==SPB], cex=2, col=col_SPB)
points(m1[1],m1[2], cex=2, pch=19)
points(m2[1],m2[2], cex=2, pch=19)
points(x,y, type="l", col="red", lty=1, lwd=3)

#Plot the probability density function (pdf) along the ridgeline manifold (see Fig. 2a in Ray and Lindsay (2005)).
plot(alfa, (1/2*( dmvnorm(cbind(x,y) , m1, Z1) + dmvnorm(cbind(x,y) , m2, Z2) )), type="l", ylab="Probability density", xlab=expression(~alpha), cex.lab=1.5, cex.axis=1.5, lwd=2)

#Calculate tolerance ellipses and corresponding beta values for various points along the ridgeline manifold. Note that all the lines of the loop should be run at once.
beta_vector_SPA<-c()
beta_vector_SPB<-c()
for (i in seq(2, 1000, 1)){
#calculation of ellipses and beta for SPA. The line below is equation 2.2 in Krishnamoorty and Mathew (1999, page 236).
k<- (n_SPA-1)%*%t(c(x[i],y[i])-m1)%*%solve((n_SPA-1)*Z1)%*%(c(x[i],y[i])-m1)
p<-2
#Change the value of gamma to the desired level of statistical confidence to calculate statistical tolerance regions (see Krishnamoorty and Mathew 1999)
gamma<-0.95
#the chi quantiles and beta values are obtained using the approximation in equation 3.15 in Krishnamoorty and Mathew (1999, page 239).
chi_quantile<-k*(1/(n_SPA-1))*(qchisq(1-gamma, n_SPA-p))
beta_or_chi_probability_SPA<-pchisq(chi_quantile, p, ncp=p/n_SPA, lower.tail = TRUE, log.p = FALSE)

#calculation of ellipses and beta for SPB. The line below is equation 2.2 in Krishnamoorty and Mathew (1999, page 236).
k<- (n_SPB-1)%*%t(c(x[i],y[i])-m2)%*%solve((n_SPB-1)*Z2)%*%(c(x[i],y[i])-m2)
k
p<-2
#Change the value of gamma to the desired level of statistical confidence to calculate statistical tolerance regions (see Krishnamoorty and Mathew 1999)
gamma<-0.95
#the chi quantiles and beta values are obtained using the approximation in equation 3.15 in Krishnamoorty and Mathew (1999, page 239).
chi_quantile<-k*(1/(n_SPB-1))*(qchisq(1-gamma, n_SPB-p))
beta_or_chi_probability_SPB<-pchisq(chi_quantile, p, ncp=p/n_SPB, lower.tail = TRUE, log.p = FALSE)
#Gather in vectors the beta values for various points along the ridgeline manifold. 
beta_vector_SPA<-append(beta_vector_SPA, beta_or_chi_probability_SPA, after=length(beta_vector_SPA))
beta_vector_SPB<-append(beta_vector_SPB, beta_or_chi_probability_SPB, after=length(beta_vector_SPB))
}

#Plot the value of beta at various points along the ridgeline manifold,
#and an horizontal dashed line at beta=0.9 (=a possible a priori cutoff value of 0.1); change according to cutoff value defined a priori (see Wiens and Servedio 2000).
par(mar=c(5,5,2,2))
plot(alfa[2:1000], beta_vector_SPA, type="l", col="black", ylim=c(0,1), lwd=2, xlab=expression(~alpha), ylab=expression(paste("Proportion ",beta, " within tolerance region")), cex.axis=1.5, cex.lab=1.5)
points(alfa[2:1000], beta_vector_SPB, type="l", col="black", lty=1, lwd=2)
points(alfa[1], beta_vector_SPA[1],cex=3, pch=22)
points(alfa[1], beta_vector_SPB[1],cex=3, pch=24)
points(alfa[999], beta_vector_SPA[999],cex=3, pch=22)
points(alfa[999], beta_vector_SPB[999],cex=3, pch=24)
abline(h=0.9, lty=3)