## Introduction

This repository contains the code and data presented in:

> Zapata F, Jimenez I. 2012. Species Delimitation: Inferring Gaps in Morphology
> across Geography. [Syst. Biol.](http://sysbio.oxfordjournals.org/content/61/2/179) 61: 179-194.

## Dependencies
The scripts require [R](http://cran.r-project.org) and the following
[R packages](http://cran.r-project.org/web/packages/available_packages_by_name.html):

- library(labdsv)
- library(ellipse)
- library(mvtnorm)
- library(vegan)
- library(spdep)

## Data
The data used is included in `data_example_A.csv` and `data_example_B.csv`. Please refer
to the manuscript for details on each example.

## Running the analyses
Run `ScriptGaps.r` to estimate gaps in morphology, and `GeographicVariation.r` to evaluate
the hypothesis that gaps can be alternatively explained as morphological variation within
species.

Both scripts will regenerate the figures presented in the manuscript.
