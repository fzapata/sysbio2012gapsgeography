#Online Appendix 6 in Zapata F., Jimenez I. Species Delimitation in Systematics: Inferring Gaps in Morphology across Geography.
#R script for confronting a hypothesis of species limits against the alternative hypothesis of geographic variation within a species.

#load libraries
library(labdsv)
library(vegan)
library(spdep)

#Load data for example A (or for example B by changing line below to read file "OnlineAppendix4_data_example_B.csv").
data <- read.csv(file="data_example_A.csv", sep=",", header=T)

#Run principal component analysis on correlation matrix (i.e., on standardized variables), selecting the appropriate columns in the original data matrix. For example B, change line below to select the variables from columns 14 to 40.
PA <- pca(data[, (14:36)], cor = T)

#Extract PC scores, bind them to the data matrix, and attach the data matrix.
data <- cbind(data, as.data.frame(PA$scores))
attach(data)

#Assign codes to the two species to be compared (e.g., 1 and 2).
SPA <- 1
SPB <- 2

#Ensure that all specimens have unique geographic coordinates so that the spatial eigenvectors
#are orthogonal. If (an only if) some specimens have the exact same coordinates, alter the coordinates
#of some specimens so that all geographic coordinates are unique. If there is need to alter coordinates,
#alter those of as few specimens as possible, and by as little as possible (e.g., only by 1 meter or less).
#This alteration is reasonable because two specimens are generally unlikely to have been collected in exactly
#the same point in geographic space.  
coor<-cbind(POINT_X[SP==SPA | SP==SPB], POINT_Y[SP==SPA | SP==SPB])
plot(coor, cex=1.5)
n_coor<-length(coor[,1])
factorial(n_coor)/(2*factorial(n_coor-2))
d<-as.matrix(dist(coor))
length(d[upper.tri(d)])
inspect_coor<-which(d==0, arr.ind=T)
nrow(inspect_coor)
(nrow(inspect_coor)-n_coor)/2
#if (and only if) the number above is higher than zero, run the next script line to look up the columns and rows
#where the non-diagonal elements in the distance matrix have zero distance. Alter the coordinates of as few
#specimens as possible, and by as little as possible, to avoid any non-diagonal zero elements in the distance matrix.
inspect_coor

#Run first RDA to detrend original response variables (PC1, PC2) using geographic coordinates as explanatory variables.
#Define response variables.
data_PC1<-PC1[SP==SPA | SP==SPB]
data_PC2<-PC2[SP==SPA | SP==SPB]
#Define explanatory variables.
X<-coor[,1]
Y<-coor[,2]
#Define the formula for RDA.
b<-c("X","Y")
formula_det<-as.formula(paste("cbind(data_PC1,data_PC2) ~ ", paste(b, collapse= "+")))
#Run and plot RDA and test significance by permutation.
rda_det_model<-rda(formula_det)
plot(rda_det_model)
anova(rda_det_model, alpha=0.05, beta=0.01, step=100, perm.max=9999)
#If (and only if) there is a siginificant linear trend, run the next script line
#to extract residuals.
det_residuals<-residuals(rda_det_model)

#Calculate spatial eigenvalues and eigenvectors.
#See Dormann et al. (2007) for details.
plot(coor, cex=1.5)
mst<-spantree(d)
lines(mst, coor)
ws<-(d<=max(mst$dist))*(1-((d/(4*(max(mst$dist))))^2))
w<-(d>0)*ws
Id<-diag(n_coor)
ones<-rep(1, n_coor)
dim(ones)<-c(n_coor, 1)
l<-ones%*%t(ones)
l_n<-l/n_coor
res1<-Id-l_n
res2<-res1%*%w
res3<-res2%*%res1
ei_sim<-eigen(res3)

#Run second RDA using eigenvectors associated with positive eigenvalues,
#dummy variables for each species, and their interactions as explanatory variables.
#Use either original variables or multivariate residuals as response variables, depending
#on wether the first RDA yielded a siginificant linear trend.
#List and enumerate eigenvectors associated with positive eigenvalues.
pos_eivec<-seq(1:length(ei_sim$values))[ei_sim$values>0]
n_pos_eivec<-length(ei_sim$values[ei_sim$values>0])
#Define dummy variables and their interactions.
dum<-as.numeric(SP[SP==SPA | SP==SPB]==SPA)
interactions<-as.matrix(ei_sim$vectors[,1:n_pos_eivec]*dum)
#Define the formula for RDA.
a<-c(paste("ei_sim$vectors[,", pos_eivec, "]", sep=""), "dum", paste("interactions[,", c(1:n_pos_eivec), "]", sep=""))
#If there is no significant linear trend in first RDA, then run next script line to use original PCA axes as response variables.
formula1<-as.formula(paste("cbind(data_PC1,data_PC2) ~ ", paste(a, collapse= "+")))
#If there was a significant linear trend in  first RDA, then run the next script line to use residuals as response variables.
formula1<-as.formula(paste("det_residuals ~ ", paste(a, collapse= "+")))
#Run and plot RDA, and test significance of different terms by permutation.
rda_model1<-rda(formula1)
plot(rda_model1)
anova(rda_model1, alpha=0.05, beta=0.01, step=100, perm.max=9999, by = "terms")

#The lines below apply standard model simplification procedures (Crawley 2002).
rda_model2<-update(rda_model1, ~.-interactions[, 1])
anova(rda_model2, alpha=0.05, beta=0.01, step=100, perm.max=9999, by = "terms")

#The lines below can be used to obtain adjusted redundancy statistic (Peres-Neto et al. 2006)
d<-c(paste("ei_sim$vectors[,", pos_eivec, "]", sep=""))
e<-c("dum", paste("interactions[,", c(1:n_pos_eivec), "]", sep=""))
#If there is no significant linear trend in first RDA, then run next script line to use original PCA axes as response variables.
formula2<-as.formula(paste("cbind(data_PC1,data_PC2) ~ ", paste(d, collapse= "+")))
formula3<-as.formula(paste("cbind(data_PC1,data_PC2) ~ ", paste(e, collapse= "+")))
#If there was a significant linear trend in  first RDA, then run the next script line to use residuals as response variables.
formula2<-as.formula(paste("det_residuals ~ ", paste(d, collapse= "+")))
formula2
formula3<-as.formula(paste("det_residuals ~ ", paste(e, collapse= "+")))
formula3
#Update formula 2 or 3 according to simplification of rda_model
formula3a<-update(formula3, ~.-interactions[,1])
formula3a
#calculate adjusted R-squared.
variance_explained<-varpart(det_residuals, formula2, formula3)
variance_explained
